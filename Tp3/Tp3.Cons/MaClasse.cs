﻿using System;

namespace Tp3.Cons
{
    public class MaClasse
    {
        public MaClasse()
        {
            Console.WriteLine("Constructeur");
        }

        ~MaClasse()
        {
            Console.WriteLine("Destructeur");
        }
    }
}
