﻿using System;

namespace Tp3.Cons
{
    class Program
    {
        static void Main(string[] args)
        {
            MaClasse maclasse = new MaClasse();
            Console.WriteLine(maclasse);


            Color macouleur = new Color(255, 123, 78);
            Console.WriteLine(macouleur);
            Console.WriteLine(macouleur.Black);


            Voiture mavoiture = new Voiture(2,5,15);
            Console.WriteLine(mavoiture.GetInfosTechniques());
            Console.WriteLine(mavoiture.SeDeplacer());

            mavoiture.DegreSalissure = 10;

            Console.WriteLine(mavoiture.DegreSalissure);
            Station.LavageStation Lavage = Station.LavageFaible;
            Lavage(mavoiture);

            Console.ReadKey();
        }
    }
}
