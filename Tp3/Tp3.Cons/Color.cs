﻿using System;

namespace Tp3.Cons
{
    public class Color
    {
        private byte m_rouge;
        private byte m_vert;
        private byte m_bleu;
        protected static byte m_maxintensity = 255;
        

        public byte Rouge { get => m_rouge; set { this.m_rouge = value; } }
        public byte Bleu { get => m_bleu; set { this.m_bleu = value; } }
        public byte Vert { get => m_vert; set { this.m_vert = value; } }
        public byte MaxIntensity{ get => m_maxintensity; }


        public static readonly Color White = new Color(m_maxintensity, m_maxintensity, m_maxintensity);
        public static readonly Color Black = new Color(0, 0, 0);
        public static readonly Color Blue = new Color(0, 0, m_maxintensity);

        public Color(byte p_rouge, byte p_vert, byte p_bleu)
        {
            m_rouge = p_rouge;

            m_vert = p_vert;

            m_bleu = p_bleu;
        }

        public override string ToString()
        {
            return $"ta couleur donne : {m_rouge} {m_vert} {m_bleu}";
        }

    }
}
