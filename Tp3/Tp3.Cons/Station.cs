﻿using System;
using Tp3.Cons;

namespace Tp3.Cons
{
    public class Station
    {
        public delegate void LavageStation(Voiture p_voiture);
        public static void Lavage(Voiture p_mavoiture, byte p_degrelavage)
        {
            if (p_mavoiture.DegreSalissure >= p_degrelavage)
            {
                p_mavoiture.DegreSalissure -= p_degrelavage;
            }
            else
            {
                p_mavoiture.DegreSalissure = 0;
            }
        }
        public static void LavageFaible(Voiture p_mavoiture)
        { Lavage(p_mavoiture, 2);}
        public static void LavageNormal(Voiture p_mavoiture)
        { Lavage(p_mavoiture, 4);}
        public static void LavageIntense(Voiture p_mavoiture)
        {Lavage(p_mavoiture, 7); }
        public static void LavageDeluxe(Voiture p_mavoiture)
        {Lavage(p_mavoiture, 9); }
    }
}
