﻿using System;

namespace Tp3.Cons
{
    public class Voiture : Vehicule
    {
        private byte m_degresalissure;

        public byte DegreSalissure
        {
            get => m_degresalissure;
            set{  m_degresalissure = value; }
        }
        public Voiture(byte p_nb_passenger, int p_nb_capacity, byte p_degresalissure)
        {
            m_degresalissure = p_degresalissure;     
        }

        public override sealed string SeDeplacer()
        {
            return "je roule";
        }
    }
}
