﻿using System;

namespace Tp3.Cons
{
    public abstract class Vehicule
    {
        private string m_type;
        private byte m_nbpassager;
        private int m_nbcapacity;


        public string Type { get => m_type; }   
        public byte NbPassager { get => m_nbpassager; }
        public int NbCapacity { get => m_nbcapacity; }

        

        public Vehicule(string p_type, byte p_nbpassenger, int p_nbcapacity)
        {
            m_type = p_type;
            if (p_nbpassenger > 10)
                throw new ArgumentException(" belle voiture !");
            m_nbpassager = p_nbpassenger;

            if (p_nbcapacity > 200)
                throw new ArgumentException("pas trop !! ");
            m_nbcapacity = p_nbcapacity;

        }

        public abstract string SeDeplacer();

        public virtual string GetInfosTechniques()
        {
            return String.Format($"Type : {m_type}\nNbPassenger : {m_nbpassager}\nNbCapacity : {m_nbcapacity}\n");
        }
    }
}
