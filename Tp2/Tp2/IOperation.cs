using System;

namespace Tp2
{
    interface IOperation
    {
        void Demarrer();

        void Eteindre();

        bool EstEnMarche { get; }

    }
}

    