﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tp2
{
    class Program
    {
        static void Main(string[] args)
        {
            void Question()
            {
                //Q1: timer
                DateTime m_time = new DateTime();
                Console.WriteLine("Le timer :" + m_time + "\n");

                //Q2: variable
                Console.WriteLine("Les changement de variables :");
                uint monuint = 1; Console.WriteLine(monuint);
                ulong monulong = 2; Console.WriteLine(monulong + "\n");
                uint tampon = 0;

                tampon = monuint;
                monuint = (uint)monulong; Console.WriteLine(monuint);
                monulong = (ulong)tampon; Console.WriteLine(monulong + "\n");
            }

            //EXO1 boucles
            void Exo1()
            {

                Console.WriteLine("La boucle : \n");
                for (int i = 0; i < 10; i++)
                    Console.Write(i);
                Console.WriteLine("\n");

                for (int i = 0; i < 10; i++)
                    if (i > 6)
                        Console.Write(i);
                Console.WriteLine("\n");

                for (int i = 0; i < 10; i++)
                    if (i < 5)
                        Console.Write(i);
                Console.WriteLine("\n");
            }

            //EXO2
            void Exo2()
            {

                Console.WriteLine("La saisie : \n");
                string lasaisie;
                do
                    lasaisie = Console.ReadLine();
                while (lasaisie.Substring(0, 1) == "a");
            }

            //EXO3
            void Exo3()
            {

                Console.WriteLine("les tableaux : \n");
                string[] v_tabdesjours = { "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche" };
                foreach (string v_jour in v_tabdesjours)
                {
                    Console.WriteLine(v_jour);
                }
            }

            //EXO4
            void Exo4()
            {
                try { 
                    int v_result;
                    do
                    {
                        string v_a = Console.ReadLine();
                        byte v_premiernum = Convert.ToByte(v_a);

                        string v_b = Console.ReadLine();
                        byte v_secondnum = Convert.ToByte(v_b);

                        v_result = v_premiernum + v_secondnum;
                        Console.WriteLine(v_result);
                    }
                    while (v_result < 50);
                }
                catch (FormatException  v_ex)
                {
                    throw new Exception($"erreur à la saisie {v_ex}");
                }
               
            }

            void Exo5()
            {
                Lampe v_lamp = new Lampe();
                Console.WriteLine(v_lamp);

            }
           int v_numero;
           string v_numdelexo;
           do
           {
                Console.WriteLine("Quel exo ? (1,2,3,4 ou 5)");

                v_numdelexo = Console.ReadLine();
                v_numero = Convert.ToInt32(v_numdelexo);
            
                    switch (v_numero)
                    {
                        case 0: break;
                        case 1: Exo1(); break;
                        case 2: Exo2(); break;
                        case 3: Exo3(); break;
                        case 4: Exo4(); break;
                        case 5: Exo5(); break;
                    }
            }
            while(v_numero != 0);
            
            Console.ReadKey();
        }
    }
}
